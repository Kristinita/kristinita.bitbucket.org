Title: Саша идеальна
Date: 2016-01-20 15:00
Category: Разное
Slug: Life-hacks\Sasha-Idealna
Tags: блог, разное, тест, sasha
Summary: Еще одна рыба, просто для массовости.
SashaGoddess: Ideal


http://duncanlock.net/blog/2013/05/17/how-i-built-this-website-using-pelican-part-1-setup/

http://hyde.getpoole.com/ — 2-столбчатая тема для сайта

http://nafiulis.me/making-a-static-blog-with-pelican.html — ещё про Пеликан для начинающих

https://github.com/getpelican/pelican-plugins — плагины для pelican

http://stackoverflow.com/a/41316723/5951529 — не работает quickstart

http://romeogolf.site/pelican-i-napolnenie-zagotovki-saita-khot-chem-nibud.html — блог о Пеликан

https://the-bosha.ru/2016/05/04/generator-staticheskih-blogov-pelican/ — плагины для Pelican

https://www.sitepoint.com/7-reasons-not-use-static-site-generator/ — почему НЕ использовать статические сайты

http://meli-lewis.github.io/blog/ — красивый шрифт

http://stackoverflow.com/a/36266002/5951529 — fabric3, а не fabric для Python3 нужен

http://docs.getpelican.com/en/stable/themes.html#article-html — как добавлять собственные переменные

{{ page.content }} — куда будет выводиться основной контент страницы
