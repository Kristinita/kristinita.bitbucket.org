Title: SashaSublime
Category: Sublime Text
Tags: Sublime Text
Slug: Sashula
Status: published
Author: Sasha Chernykh
PageTitle: SashaSublime
MetaContent: SashaSublime — theme and color scheme for Sublime Text 3, where all elements are good visible
Colors: sublime-text
IconLeftOrRight: right
